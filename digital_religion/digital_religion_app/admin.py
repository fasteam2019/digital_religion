from django.contrib import admin
from django.forms import Media, CharField
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from .forms import AdminNewForm
from .models import Pages, Categories, News, Comments

admin.site.register(Pages)
admin.site.register(Categories)
admin.site.register(Comments)


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    exclude = ['owner']
    content = CharField(widget=CKEditorUploadingWidget())

    def _media(self):
        js = ['/static/js/jquery-3.2.1.min.js','/static/js/filterCategory.js', ]
        return Media(js=js)

    media = property(_media)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        super().save_model(request, obj, form, change)
