from django.urls import path, include
from django.views.generic import TemplateView

from . import views, endpoints

app_name = "digital_religion"

urlpatterns = [
    path("getCategoriesByPageId", views.get_category_by_pages, name="get_categories_by_page_id"),
    path("<slug:slug>", views.page_detail, name="page_detail"),
    path("<slug:pageSlug>/<slug:newsSlug>/id/<int:pk>", views.news_detail, name="news_detail"),
    path("<slug:pageSlug>/<slug:newsSlug>/<int:pk>", views.category_detail, name="category_detail"),
    # path("<slug:slug>/<slug-slug>/", TemplateView.as_view(template_name="pages/home.html")),
    path("api/", include(endpoints))

]
