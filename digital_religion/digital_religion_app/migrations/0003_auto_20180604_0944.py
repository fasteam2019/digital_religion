# Generated by Django 2.0.6 on 2018-06-04 09:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('digital_religion_app', '0002_news_page'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categories',
            name='page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='categories', to='digital_religion_app.Pages'),
        ),
    ]
