# Generated by Django 2.0.6 on 2018-06-04 14:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('digital_religion_app', '0005_pages_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pages',
            name='slug',
            field=models.SlugField(blank=True, unique=True),
        ),
    ]
