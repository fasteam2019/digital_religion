from django.conf.urls import include
from django.urls import path
from rest_framework import routers

from .api import LatestNewsViewSet, CategoriesHomeViewSet, NewsViewSet, CommentViewSet, RelatedNewsViewSet

router = routers.DefaultRouter()
router.register('latestNews', LatestNewsViewSet)
router.register('categoriesHome', CategoriesHomeViewSet)
router.register('news', NewsViewSet)
router.register('comment', CommentViewSet)
router.register('relatedNews', RelatedNewsViewSet)

urlpatterns = [
    path("", include(router.urls)),]
