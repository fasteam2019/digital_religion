from django.utils.text import slugify
from rest_framework import serializers

from .models import News, Categories, Comments


class NewsSerializer(serializers.ModelSerializer):
    news_url = serializers.SerializerMethodField()
    category_name = serializers.SerializerMethodField()

    class Meta:
        fields = (
            'id',
            'title',
            'short_description',
            'thumbnail',
            'news_url',
            'created',
            'category_name',
            'content',
        )
        model = News

    def get_news_url(self, obj):
        return '/{0}/{1}/id/{2}'.format(obj.page.slug, slugify(obj.title), obj.pk)

    def get_category_name(self, obj):
        return obj.category.name


class CategoriesHomeSerializer(serializers.ModelSerializer):
    news_list = NewsSerializer(read_only=True, many=True)
    category_url = serializers.SerializerMethodField()

    class Meta:
        model = Categories
        fields = ('id', 'name', 'news_list', 'category_url')

    def get_category_url(self, obj):
        return '{0}/{1}/{2}'.format(obj.page.slug, slugify(obj.name), obj.pk)


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        fields = (
            'id',
            'title',
        )
        model = News

    def get_news_url(self, obj):
        return '{0}/{1}/{2}'.format(obj.page.slug, slugify(obj.title), obj.pk)


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        fields = ['full_name', 'email', 'message', 'news', 'created']
