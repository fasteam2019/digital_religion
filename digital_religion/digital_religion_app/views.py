from django.core import serializers
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from digital_religion.digital_religion_app.models import Categories, Pages, News


@csrf_exempt
def get_category_by_pages(request):
    if request.is_ajax():
        page_id = request.GET['page_id']
        categories = Categories.objects.filter(page=Pages.objects.get(pk=page_id))
        return HttpResponse(serializers.serialize('json', categories), content_type='application/json')


def page_detail(request, slug):
    print(slug)
    try:
        Pages.objects.get(slug=slug)
    except Pages.DoesNotExist:
        raise Http404("Page does not exist")
    return render(request, "pages/home.html", {})


def news_detail(request, pageSlug, newsSlug, pk):
    try:
        News.objects.get(pk=pk)
    except Pages.DoesNotExist:
        raise Http404("Page does not exist")
    return render(request, "pages/home.html", {})


def category_detail(request, pageSlug, categorySlug, pk):
    try:
        Categories.objects.get(pk=pk)
    except Pages.DoesNotExist:
        raise Http404("Page does not exist")
    return render(request, "pages/home.html", {})



