from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.db import models
from model_utils.models import TimeStampedModel


class Pages(models.Model):
    name = models.CharField(max_length=255, null=False)
    slug = models.SlugField(unique=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Pages'


class Categories(models.Model):
    page = models.ForeignKey(Pages, null=False, related_name="categories", on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'


class News(TimeStampedModel):
    page = models.ForeignKey(Pages, null=False, on_delete=models.CASCADE)
    category = models.ForeignKey(Categories, related_name='news_list', null=False, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, null=False)
    short_description = models.TextField(blank=True)
    content = RichTextUploadingField()
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE)
    source = models.CharField(max_length=255, blank=True)
    thumbnail = models.ImageField(upload_to='upload/images', default='no_image.jpg')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'News'


class Comments(TimeStampedModel):
    PENDING = 0
    APPROVED = 1
    UNAPPROVED = 2
    STATUS = (
        (PENDING, 'Chờ duyệt'),
        (APPROVED, 'Đã duyệt'),
        (UNAPPROVED, 'Từ chối')
    )
    news = models.ForeignKey(News, null=False, on_delete=models.CASCADE)
    email = models.CharField(max_length=255, null=False)
    full_name = models.CharField(max_length=255, null=False)
    message = models.TextField(null=False)
    status = models.IntegerField(choices=STATUS, default=PENDING)

    def __str__(self):
        return self.news.title + '-' + self.email

    class Meta:
        verbose_name_plural = 'Comments'
