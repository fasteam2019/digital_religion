from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import News, Categories, Comments
from .serializers import NewsSerializer, CategorySerializer, CategoriesHomeSerializer, CommentSerializer


class LatestNewsViewSet(ModelViewSet):
    queryset = News.objects.all()
    permission_classes = [permissions.AllowAny, ]
    serializer_class = NewsSerializer

    def get_queryset(self):
        latest_news_list = News.objects.filter(page__slug='tin-tuc-su-kien').order_by('-created')[:5]
        return latest_news_list

class CategoriesHomeViewSet(ModelViewSet):
    queryset = Categories.objects.all()
    permission_classes = [permissions.AllowAny, ]
    serializer_class = CategoriesHomeSerializer

    def get_queryset(self):
        return Categories.objects.filter(page__slug='tin-tuc-su-kien')[:4]

class NewsViewSet(ModelViewSet):
    queryset = News.objects.all()
    permission_classes = [permissions.AllowAny, ]
    serializer_class = NewsSerializer


class CommentViewSet(ModelViewSet):
    queryset = Comments.objects.filter(status=Comments.APPROVED)
    permission_classes = [permissions.AllowAny, ]
    serializer_class = CommentSerializer

    def get_queryset(self):
        newsId= self.request.query_params.get('newsId', -1)
        return Comments.objects.filter(status=Comments.APPROVED, news_id=newsId)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class RelatedNewsViewSet(ModelViewSet):
    queryset = News.objects.all()
    permission_classes = [permissions.AllowAny, ]
    serializer_class = NewsSerializer

    def get_queryset(self):
        newsId= self.request.query_params.get('newsId', -1)
        news = News.objects.get(pk=newsId)
        return News.objects.filter(category=news.category).exclude(id=newsId).order_by('-created')[:3]



