from django.apps import AppConfig


class DigitalReligionAppConfig(AppConfig):
    name = 'digital_religion.digital_religion_app'
    verbose_name = 'Digital Region'
