from django.forms import ModelForm, Media

from .models import News


class AdminNewForm(ModelForm):
    class Meta:
        model = News
        exclude = ('owner',)

    def _media(self):
        js = ['/static/js/filterCategory.js', ]
        return Media(js=js)

    media = property(_media)
