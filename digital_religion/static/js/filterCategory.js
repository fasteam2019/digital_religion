$(document).ready(() => {
    $('#id_page').on('change', (e) => {
        $.ajax({
            method: 'GET',
            url: `/getCategoriesByPageId?page_id=${e.target.value}`,
            success: (categories) => {
                $('#id_category').empty();
                $('#id_category').append('<option>---------</option>')
                for (let category of categories) {
                    $('#id_category').append(`<option value=${category.pk}>${category.fields.name}</option>`)
                }
            }
        });
    });
})
