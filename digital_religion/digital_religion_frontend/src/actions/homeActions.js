export const fetchLatestNews = () => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        return fetch("/api/latestNews", {headers,})
            .then(res => {
                if (res.status === 200) {
                    return res.json().then(data => {
                        return {status: res.status, data}
                    })
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({type: 'FETCH_LATEST_NEWS', latestNews: res.data})
                }
            })
    }
};

export const fetchCategoriesHome = () => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        return fetch("/api/categoriesHome", {headers,})
            .then(res => {
                if (res.status === 200) {
                    return res.json().then(data => {
                        return {status: res.status, data}
                    })
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({type: 'FETCH_CATEGORIES_HOME', categoriesHome: res.data})
                }
            })
    }
};

export const fetchNewsDetail = newsId => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        return fetch(`/api/news/${newsId}/`, {headers,})
            .then(res => {
                if (res.status === 200) {
                    return res.json().then(data => {
                        return {status: res.status, data}
                    })
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({type: 'FETCH_NEWS_DETAIL', newsDetail: res.data})
                }
            })
    }
};

export const fetchComments = newsId => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        return fetch(`/api/comment/?newsId=${newsId}`, {headers,})
            .then(res => {
                if (res.status === 200) {
                    return res.json().then(data => {
                        return {status: res.status, data}
                    })
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({type: 'FETCH_COMMENTS', comments: res.data})
                }
            })
    }
};

export const addComment = (newsId, fullName, email, message) => {
  return dispatch => {
    let headers = {"Content-Type": "application/json"};
    let body = JSON.stringify({'news': parseInt(newsId), 'full_name': fullName, email, message});
    return fetch("/api/comment/", {headers, method: "POST", body})
      .then(res => res.json())
      .then(comment => {
        return dispatch({
          type: 'ADD_COMMENT',
          comment
        })
      })
  }
};

export const fetchRelatedNews = newsId => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        return fetch(`/api/relatedNews/?newsId=${newsId}`, {headers,})
            .then(res => {
                if (res.status === 200) {
                    return res.json().then(data => {
                        return {status: res.status, data}
                    })
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({type: 'FETCH_RELATED_NEWS', relatedNews: res.data})
                }
            })
    }
};
