import React, {Component} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";

import {connect} from "react-redux";
import NotFound from "../components/NotFound";

import Header from "../components/Header";
import Home from "../components/Home";
import PageDetail from "../components/PageDetail";
import NewsDetail from "../components/NewsDetail";
import Footer from "../components/Footer";

class RootContainer extends Component {

    render() {
        return (
            <BrowserRouter>
                <div>
                    <Header/>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        {/*<Route exact path="/gioi-thieu" component={Introduction}/>*/}
                        {/*<Route exact path="/tin-tuc-su-kien" component={Introduction}/>*/}
                        {/*<Route exact path="/van-ban-cua-dang-nha-nuoc" component={Introduction}/>*/}
                        {/*<Route exact path="/bo-thu-tuc-hanh-chinh" component={Introduction}/>*/}
                        {/*<Route exact path="/trao-doi-nghiep-vu" component={Introduction}/>*/}
                        {/*<Route exact path="/lien-he" component={Introduction}/>*/}
                        <Route exact path="/:pageSlug" component={PageDetail} />
                        <Route exace path="/:pageSlug/:newsSlug/id/:newsId" component={NewsDetail} />
                        <Route component={NotFound}/>
                    </Switch>
                    <Footer/>
                </div>
            </BrowserRouter>
        );
    }
}

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
