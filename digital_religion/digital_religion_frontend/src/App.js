import React, {Component} from "react";
import "./App.css";
import RootContainer from "./containers/RootContainer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <RootContainer/>
      </div>
    );
  }
}

export default App;
