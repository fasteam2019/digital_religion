import {combineReducers} from 'redux';
import homeReducers from "./homeReducers";


const digitalReligionApp = combineReducers({
    homeReducers
})

export default digitalReligionApp;
