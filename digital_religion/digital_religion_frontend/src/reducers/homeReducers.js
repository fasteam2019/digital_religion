const initialState = {
    latestNewsList: [],
    firstNews: [],
    categoriesHome: [],
    newsDetail: null,
    comments: [],
    relatedNews: []
};

export default function homeReducers(state = initialState, action) {
    switch (action.type) {
        case 'FETCH_LATEST_NEWS':
            const firstNews = action.latestNews.splice(0, 1);
            return {...state, firstNews: firstNews, latestNewsList: action.latestNews};
        case 'FETCH_CATEGORIES_HOME':
            return {...state, categoriesHome: action.categoriesHome};
        case 'FETCH_NEWS_DETAIL':
            return {...state, newsDetail: action.newsDetail};
        case 'FETCH_COMMENTS':
            return {...state, comments: action.comments};
        case 'ADD_COMMENT':
            return {...state};
        case 'FETCH_RELATED_NEWS':    
            console.log(action.relatedNews);   
            return {...state, relatedNews: action.relatedNews};
        default:
            return state;
    }
}
