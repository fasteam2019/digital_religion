import React, {Component} from "react";
import logo from "../logo.svg";
import {NavLink} from "react-router-dom";

export default class Footer extends Component {
    render() {
        return (
            <div className="footer container">
                <div className="m-b-1">
                    <nav className="navbar navbar-expand-md navbar-light">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <NavLink exact={true} to="/" className="nav-link" activeClassName='is-active'>Trang chủ</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/gioi-thieu'>Giới thiệu</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/tin-tuc-su-kien'>Tin tức sự kiện</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/van-ban-cua-dang-nha-nuoc'>Văn bản của Đảng, Nhà nước</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/bo-thu-tuc-hanh-chinh'>Bộ thủ tục hành chính</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/trao-doi-nghiep-vu'>Trao đổi nghiệp vụ</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/lien-he'>Liên hệ</NavLink>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="footer-info p-3">
                    <p>TRANG THÔNG TIN ĐIỆN TỬ BAN TÔN GIÁO THÀNH PHỐ ĐÀ NẴNG</p>
                    <p>Người chịu trách nhiệm: Ông </p>
                    <p>Giấy phép số </p>
                    <p>Bản quyền thuộc Ban Tôn giáo thành phố Đà Nẵng</p>
                    <p>Địa chỉ: 70 Quang Trung, phường Thạch Thang, quận Hải Châu, thành phố Đà Nẵng</p>
                    <p>Điện thoại: 0236 33812866. Email: btg@danang.gov.vn</p>
                    <p>Ghi rõ nguồn: "Trang thông tin điện tử Ban Tôn giáo thành phố Đà Nẵng" hoặc "www.btgdn.com.vn" khi phát hành lại thông tin từ các nguồn này</p>
                </div>
            </div>
        )
    }
}
