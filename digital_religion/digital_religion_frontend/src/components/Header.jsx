import React, {Component} from "react";
import logo from "../logo.svg";
import {NavLink} from "react-router-dom";
import Moment from 'react-moment';

export default class Header extends Component {
    render() {
        return (
            <div className="header container">
                <div className="header-inside">
                    <img src={logo} className="App-logo" alt="logo"/>
                <div className="m-b-1">
                    <nav className="navbar navbar-expand-md navbar-light">
                        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <NavLink exact={true} to="/" className="nav-link" activeClassName='is-active'>Trang chủ</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/gioi-thieu'>Giới thiệu</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/tin-tuc-su-kien'>Tin tức sự kiện</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/van-ban-cua-dang-nha-nuoc'>Văn bản của Đảng, Nhà nước</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/bo-thu-tuc-hanh-chinh'>Bộ thủ tục hành chính</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/trao-doi-nghiep-vu'>Trao đổi nghiệp vụ</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' activeClassName='is-active' to='/lien-he'>Liên hệ</NavLink>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="date-search row justify-content-between pt-2 pb-2">
                    <div className="col-md-auto">
                        <Moment format="DD-MM-YYYY HH:mm"></Moment>
                    </div>
                    <div className="col-md-auto">
                        Search bar
                    </div>
                </div>
                </div>
            </div>
        )
    }
}
