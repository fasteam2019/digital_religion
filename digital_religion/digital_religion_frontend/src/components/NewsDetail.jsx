import React, {Component} from 'react';
import NewsDetailSection from "./NewsDetailSection";
import CommentSection from "./CommentSection";
import RelatedNewsSection from "./RelatedNewsSection";

export default class NewsDetail extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-9 main-container">
                        <NewsDetailSection newsId={this.props.match.params.newsId}/>
                        <CommentSection newsId={this.props.match.params.newsId}/>
                        <RelatedNewsSection newsId={this.props.match.params.newsId}/>
                    </div>
                    <div className="col-md-3">

                    </div>
                </div>
            </div>
        )
    }
}
