import React, { Component } from 'react';

export default class PageDetail extends Component {
    render() {
        return (
            <p className="App-intro">
                Page: {this.props.match.params.pageSlug}
            </p>
        )
    }
}
