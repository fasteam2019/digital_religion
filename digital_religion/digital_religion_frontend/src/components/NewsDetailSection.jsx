import React, {Component} from 'react';
import {connect} from "react-redux";
import {homeActions} from "../actions";
import Moment from 'react-moment';
import 'moment/locale/vi';

class NewsDetailSection extends Component {

    componentDidMount() {
        this.props.fetchNewsDetail(this.props.newsId);

    }

    render() {
        if (!this.props.newsDetail) {
            return (
                <div className="news-detail-section">
                    <div>Loading ...</div>
                </div>
            )
        }
        else {
            const newsDetail = this.props.newsDetail;
            return (
                <div className="news-detail-section pt-3">
                    <h3 className="text-primary">{newsDetail.category_name}</h3>
                    <h1>{newsDetail.title}</h1>
                    <div className="fine-print"><Moment locale="vi" format="DD-MM-YYYY HH:mm">{newsDetail.created}</Moment></div>
                    <p className="news-description">{newsDetail.short_description}</p>
                    <div dangerouslySetInnerHTML={{__html: newsDetail.content}}/>
                    <p className="news-source">{newsDetail.source}</p>
                </div>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        newsDetail: state.homeReducers.newsDetail
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchNewsDetail: (newsId) => {
            dispatch(homeActions.fetchNewsDetail(newsId));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsDetailSection)


