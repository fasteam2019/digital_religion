import React, {Component} from 'react';
import {connect} from "react-redux";
import {homeActions} from "../actions";

class LatestNews extends Component {

    componentDidMount() {
        this.props.fetchLatestNews();
    }

    render() {

        return (
            <div className="latest-news row pt-3 pb-3">
                <div className="latest-news-left col-md-7">
                    <a href="/tin-tuc-su-kien"><h2>Tin tức sự kiện</h2></a>
                    {this.props.firstNews.map((news, id) => (
                        <div key={`firstNews_${id}`}>
                            <a href={news.news_url} style={{display: 'block'}} className="mb-2">
                                <img src={news.thumbnail} alt="News"/>
                            </a>
                            <a href={news.news_url}>
                                <h3>{news.title}</h3>
                            </a>
                            <div>{news.short_description}</div>
                        </div>
                    ))}
                </div>
                <div className="latest-news-right col-md-5">
                    <a href="/tin-tuc-su-kien"><h2>Tin tức sự kiện</h2></a>
                    {this.props.latestNewsList.map((news, id) => (
                        <div key={`news_${news.id}`} className="latest-news-right-element mb-2">
                            <a className="wp-image mr-2" href={news.news_url}>
                                <img src={news.thumbnail} alt="News"/>
                            </a>
                            <a href={news.news_url}>
                                <div>{news.title}</div>
                            </a>
                        </div>

                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        latestNewsList: state.homeReducers.latestNewsList,
        firstNews: state.homeReducers.firstNews
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchLatestNews: () => {
            dispatch(homeActions.fetchLatestNews());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LatestNews)


