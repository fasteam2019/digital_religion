import React, {Component} from "react";
import LatestNews from "./LatestNews";
import CategoriesHomeSection from "./CategoriesHomeSection";

export default class Home extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-9 main-container">
                        <LatestNews/>
                        <CategoriesHomeSection/>
                    </div>
                    <div className="col-md-3">

                    </div>
                </div>
            </div>
        )
    }
}
