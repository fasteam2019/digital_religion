import React, {Component} from 'react';
import {connect} from "react-redux";
import {homeActions} from "../actions";
import Moment from 'react-moment';
import 'moment/locale/vi';
import Modal from 'react-modal';
import {FormWithConstraints, FieldFeedbacks, FieldFeedback} from 'react-form-with-constraints';
import userImage from "../no-user-image.gif";

Modal.setAppElement('#root');

class CommentSection extends Component {

    componentDidMount() {
        this.props.fetchComments(this.props.newsId);
    }

    submitComment = (e) => {
        e.preventDefault();

        this.form.validateFields();

        if (this.form.isValid()) {
            this.props.addComment(this.props.newsId, this.state.fullName, this.state.email, this.state.message).then(this.openModal);
            this.resetForm();
        }
    };

    openModal = () => {
        this.setState({modalIsOpen: true})
    };

    closeModal = () => {
        this.setState({modalIsOpen: false})
    };

    handleChange = (e) => {
        this.form.validateFields(e.currentTarget);
        this.setState({[e.target.name]: e.target.value})
    };

    resetForm = () => {
        this.setState({
            fullName: "",
            email: "",
            message: "",
        })
    };

    state = {
        fullName: "",
        email: "",
        message: "",
        modalIsOpen: false,
    };

    render() {
        return (
            <div className="comment-section mb-3">
                <div className="comment-list col">
                    <h3 className="row">Bình luận</h3>
                    {this.props.comments.map((comment, id) => (
                        <div key={`comment_${id}`} className="comment-item mb-2 pt-3 pb-3 row">
                            <div className="col-md-auto user-image">
                                <img src={userImage}/>
                            </div>
                            <div className="col">
                                <div className=" row justify-content-between">
                                    <h4 className="col-md-auto text-primary">{comment.full_name}</h4>
                                    <div className="col-md-auto fineprint"><Moment locale="vi"
                                                                                   format="DD-MM-YYYY HH:mm">{comment.created}</Moment>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        {comment.message}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <div className="comment-form pb-3">
                    <h3>Ý kiến bạn đọc</h3>
                    <FormWithConstraints ref={form => this.form = form} onSubmit={this.submitComment} noValidate>
                        <div className="form-group">
                            <input name="fullName" type="text" className="form-control" placeholder="Họ và tên" required
                                   onChange={this.handleChange} value={this.state.fullName}/>
                            <FieldFeedbacks for="fullName">
                                <FieldFeedback when="valueMissing">Trường bắt buộc nhập</FieldFeedback>
                            </FieldFeedbacks>
                        </div>

                        <div className="form-group">
                            <input name="email" type="email" className="form-control" placeholder="Email" required
                                   onChange={this.handleChange} value={this.state.email}/>
                            <FieldFeedbacks for="email">
                                <FieldFeedback when="*">Địa chỉ email không hợp lệ</FieldFeedback>
                            </FieldFeedbacks>
                        </div>
                        <div className="form-group">
                        <textarea name="message" type="text" className="form-control" rows="3"
                                  placeholder="Ý kiến bạn đọc" required
                                  onChange={this.handleChange} value={this.state.message}/>
                            <FieldFeedbacks for="message">
                                <FieldFeedback when="*">Trường bắt buộc nhập</FieldFeedback>
                            </FieldFeedbacks>
                        </div>

                        <input type="submit" value="Gửi" className="btn btn-primary pl-5 pr-5"/>
                    </FormWithConstraints>

                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onRequestClose={this.closeModal}
                        contentLabel="Example Modal"
                        className="comment-modal"
                    >
                        <h3>Thông báo</h3>
                        <div className="mb-3">Cảm ơn bình luận của bạn.</div>
                        <button onClick={this.closeModal} className="btn btn-primary pr-5 pl-5">Close</button>
                    </Modal>
                </div>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        comments: state.homeReducers.comments
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchComments: (newsId) => {
            dispatch(homeActions.fetchComments(newsId));
        },
        addComment: (newsId, fullName, email, message) => {
            return dispatch(homeActions.addComment(newsId, fullName, email, message))
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentSection)


