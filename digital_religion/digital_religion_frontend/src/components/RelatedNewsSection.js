import React, {Component} from 'react';
import {connect} from "react-redux";
import {homeActions} from "../actions";

class RelatedNewsSection extends Component {

    componentDidMount() {
        this.props.fetchRelatedNews(this.props.newsId);
    }

    render() {

        return (
            <div className="related-news-section pb-3">
                <h3 className="text-primary">Các tin khác</h3>
                <div className="related-news-list row">
                    {this.props.relatedNews.map((news, id) => (
                        <div key={`news_${id}`} className="col-md-4">
                            <div className="">
                                <a href={news.news_url} className="mb-2" style={{display: 'block'}}>
                                    <img src={news.thumbnail} className="related-news-image" />
                                </a>
                            </div>
                            <div className="">
                                <a href={news.news_url}>
                                    <div className="text-primary">{news.title}</div>
                                </a>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        relatedNews: state.homeReducers.relatedNews
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchRelatedNews: (newsId) => {
            dispatch(homeActions.fetchRelatedNews(newsId));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RelatedNewsSection)


