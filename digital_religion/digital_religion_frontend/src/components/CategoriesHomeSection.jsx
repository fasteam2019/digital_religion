import React, {Component} from 'react';
import {connect} from "react-redux";
import {homeActions} from "../actions";

class CategoriesHomeSection extends Component {

    componentDidMount() {
        this.props.fetchCategoriesHome();
    }

    render() {

        return (
            <div className="categories-home-section row">
                {this.props.categoriesHome.map((category, id) => (
                    <div key={`category_${id}`} className="category-item col-md-6">
                        <div className="category-item-content pt-3 pb-3">
                            <a href={category.category_url}><h2>{category.name}</h2></a>
                            {category.news_list.map((news, news_id) => (
                                (news_id === 0) ?
                                    <div key={`news_${news_id}`} className="first-news-item mb-2">
                                        <a href={news.news_url} className="category-item-wp-image mr-2"><img src={news.thumbnail}/></a>
                                        <a href={news.news_url}>{news.title}</a>
                                    </div> :
                                    <div key={`news_${news_id}`} className="mb-1">
                                        <a href={news.news_url}>{news.title}</a>
                                    </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        categoriesHome: state.homeReducers.categoriesHome
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchCategoriesHome: () => {
            dispatch(homeActions.fetchCategoriesHome());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesHomeSection)


